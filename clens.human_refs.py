import subprocess
import os

manual = []

# Set path to current directory
path = os.getcwd()
sums_path = path + '/human_sums.txt'


def check_prior(file):
	prior = str(subprocess.check_output([f'find . -name {file}'], shell=True))
	if file in prior:
		return True
	else:
		return False


def reference_check(i):
	with open(sums_path, 'r') as f:
		if i in f.read():
			print('File successfully validated: ' + str(i).rsplit(' ', 1)[1])
			return True
		else:
			print('File failed validation: ' + str(i).rsplit(' ', 1)[1])
			return False


def download_reference(cmd, alt=None):
	if alt == None:
		file = cmd.rsplit('/', 1)[1]
	else:
		file = alt
	attempt = 0
#   while True:
#		if attempt == 3:
#			manual.append(f'{file}')
#			return False
	subprocess.run([f'{cmd}'], shell=True)
#		check = str(subprocess.check_output([f'md5sum {file}'], shell=True)).split("'", 1)[1].rsplit("\\", 1)[0]
#		if reference_check(check):
	return True
#		else:
#			subprocess.run([f'rm {file}'], shell=True)
#			attempt += 1


### Genomic reference ###

subprocess.run([f'mkdir -p {path}/references/homo_sapiens'], shell=True)
os.chdir(f'{path}/references/homo_sapiens')

if not check_prior('Homo_sapiens.assembly38.fa'):
	os.chdir(f'{path}/references/homo_sapiens')
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/fasta'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/fasta')

	if download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.fasta'):
		subprocess.run(['mv Homo_sapiens_assembly38.fasta Homo_sapiens.assembly38.fa'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: Homo_sapiens.assembly38.fa')


### GTF/GFF3 ###

if not check_prior('gencode.v36.annotation.gtf'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/annot'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/annot')

	download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_36/gencode.v36.annotation.gtf.gz')
	subprocess.run(['zcat gencode.v36.annotation.gtf.gz > gencode.v36.annotation.gtf'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: gencode.v36.annotation.gtf')


### Protein reference ###

if not check_prior('gencode.v36.pc_translations.fa'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/protein'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/protein')

	download_reference('wget ftp://ftp.ebi.ac.uk/pub/databases/gencode/Gencode_human/release_36/gencode.v36.pc_translations.fa.gz')
	subprocess.run(['gunzip gencode.v36.pc_translations.fa.gz'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: gencode.v36.pc_translations.fa.gz')


### VCF references ###

if not check_prior('1000g_pon.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/1000g_pon.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: 1000g_pon.hg38.vcf.gz')

if not check_prior('af-only-gnomad.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/af-only-gnomad.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: af-only-gnomad.hg38.vcf')

if not check_prior('Homo_sapiens_assembly38.dbsnp138.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/genomics-public-data/resources/broad/hg38/v0/Homo_sapiens_assembly38.dbsnp138.vcf')
	subprocess.run(['bgzip Homo_sapiens_assembly38.dbsnp138.vcf'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: Homo_sapiens_assembly38.dbsnp138.vcf.gz')

if not check_prior('small_exac_common_3.hg38.vcf.gz'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/vcfs'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget https://storage.googleapis.com/gatk-best-practices/somatic-hg38/small_exac_common_3.hg38.vcf.gz')
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: small_exac_common_3.hg38.vcf.gz')


### BEDs ###
 
if not check_prior('hg38.gencode_v36.exome.bed'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/beds/gencode_v36'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/beds/gencode_v36')
	subprocess.run([f'wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/ea7d0b56f0af067ea9e7a3c0e9c96c96/hg38_bed_generator.sh'], shell=True)
	subprocess.run(['bash hg38_bed_generator.sh ../../annot/gencode.v36.annotation.gtf.gz'], shell=True)
	subprocess.run(['mv hg38_exome.bed hg38.gencode_v36.exome.bed'], shell=True)
	subprocess.run(['rm bedtools_2.28.0--hdf88d34_0.sif'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens')
else:
	print('Found pre-existing file: hg38.gencode_v36.exome.bed')

### Somalier Sites reference ###
if not check_prior('somalier.sites.hg38.vcf.gz'):
	os.chdir(f'{path}/references/homo_sapiens/vcfs')
	download_reference('wget -O somalier.sites.hg38.vcf.gz https://github.com/brentp/somalier/files/3412456/sites.hg38.vcf.gz')
	os.chdir('..')
else:
	print('Found pre-existing file: snpEff.config')


### snpEff reference ###

if not check_prior('GRCh38.GENCODEv36'):
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/snpeff'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/snpeff')
	subprocess.run(['singularity pull docker://resolwebio/snpeff:latest'], shell=True)
	subprocess.run([f'mkdir -p {path}/references/homo_sapiens/snpeff/GRCh38.GENCODEv36'], shell=True)
	os.chdir(f'{path}/references/homo_sapiens/snpeff/GRCh38.GENCODEv36')

#	if download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/430f9d80c841721499fbcec937b0f721/snpEff.config'):
	if download_reference('wget https://gitlab.com/landscape-of-effective-neoantigens-software/nextflow/modules/tools/lens/-/wikis/uploads/bbcd2089ad5d4d5c5d2fcb29d0ff0829/snpEff.config'):
		subprocess.run(['ln ../../annot/gencode.v36.annotation.gtf.gz genes.gtf.gz'], shell=True)
		subprocess.run(['ln ../../fasta/Homo_sapiens.assembly38.fa sequences.fa'], shell=True)
		os.chdir('..')
		subprocess.run(['singularity exec -B $PWD snpeff*.s* /opt/snpeff/snpeff/bin/snpEff build -gtf22 -v GRCh38.GENCODEv36 -dataDir ${PWD} -c GRCh38.GENCODEv36/snpEff.config'],
					   shell=True)
		subprocess.run(['rm GRCh38.GENCODEv36/sequences.fa'], shell=True)
		subprocess.run(['rm GRCh38.GENCODEv36/genes.gtf.gz'], shell=True)
		subprocess.run(['rm snpeff*.s*'], shell=True)
		os.chdir('..')
	else:
		os.chdir(f'{path}/references/homo_sapiens')

else:
	print('Found pre-existing file: CHRCh38.GENCODEv36')

